# Laravel Project

## ¿Qué es?

Definición del framework y explicación de la estructura. (ToDo)

## Crear proyecto

Instalación de herramientas:

 https://medium.com/@panjeh/install-laravel-on-ubuntu-18-04-with-apache-mysql-php7-lamp-stack-5512bb93ab3f 

Instalar Laravel

```bash
composer global require laravel/installer
```

Crear proyecto

```bash
composer create-project --prefer-dist laravel/laravel mgpass
```

Ejecutar proyecto

```bash
# http://127.0.0.1:8000/
php artisan serve 
```

## Configuración BD

Modificar el archivo .env y colocar las respectivas credenciales.

```bash
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=dbname
DB_USERNAME=user
DB_PASSWORD=password
```

Ejecutar migraciones

```bash
php artisan migrate
```

Crear usuario mysql:

 https://stackoverflow.com/questions/36864206/sqlstatehy000-1698-access-denied-for-user-rootlocalhost 

 https://www.digitalocean.com/community/tutorials/como-instalar-y-proteger-phpmyadmin-en-ubuntu-18-04-es 

```bash
mysql -u newuser -p
create database laravel;
exit
```

## Configurar UI y Auth

 https://styde.net/paquete-laravel-ui-en-laravel-6/ 

Instalar paquete UI

```bash
composer require laravel/ui
```

Generar configuración básica, incluyendo vistas para registro y autenticación de usuarios.

```bash
php artisan ui vue --auth
```

Instalar y compilar paquetes:

```bash
npm install && npm run dev
```

## Funcionalidad Usuarios

Modelo User

```bash
php artisan make:model User -m
```

### Creando roles

Modelo Role

```bash
php artisan make:model Role -m
```

Editar la clase `CreateRolesTable` la cual se encuentra en el directorio `database/migrations/***_create_roles_table.php`

```php
...
public function up()
{
    Schema::create('roles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('description');
            $table->timestamps();
    });
}
...
```

Crear migración para asociar usuarios y roles

```bash
php artisan make:migration create_role_user_table
```

Lo anterior creará la clase `CreateRoleUserTable` en el directorio `database/migrations/***_create_role_user_table.php`, editarla y añadir lo siguiente:

```php
...
public function up()
{
    Schema::create('role_user', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('role_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->timestamps();
    });
}
...
```

#### Relación entre usuarios y roles en los modelos

Editar la clase `User` en el directorio `app/User.php`

```php
...
public function roles()
{
    return $this
        ->belongsToMany('App\Role')
        ->withTimestamps();
}
...
```

Editar el archivo `Role.php` en el directorio `app/Role.php`

```php
...
public function users()
{
    return $this
        ->belongsToMany('App\User')
        ->withTimestamps();
}
...
```

#### Lógica de autorización 

Editar la clase `User` en el directorio `app/User.php`

```php
...
public function authorizeRoles($roles)
{
    if ($this->hasAnyRole($roles)) {
        return true;
    }
    abort(401, 'Esta acción no está autorizada.');
}

public function hasAnyRole($roles)
{
    if (is_array($roles)) {
        foreach ($roles as $role) {
            if ($this->hasRole($role)) {
                return true;
            }
        }
    } else {
        if ($this->hasRole($roles)) {
            return true;
        }
    }
    return false;
}

public function hasRole($role)
{
    if ($this->roles()->where('name', $role)->first()) {
        return true;
    }
    return false;
}
...
```

Modificar el método `create()` en la clase `RegisterController` en el directorio `app/Http/Controllers/Auth/RegisterController.php`

```php
...
use App\Role;
...
    
...
protected function create(array $data)
{
	$user =  User::create([
           	'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
    ]);
    
    $user
    	->roles()
        ->attach(Role::where('name', 'user')->first());
        
    return $user;
}
...
```

Añadir en los controladores, los roles autorizados en cada métod. Por ejemplo, en el archivo `app/Http/Controllers/HomeController.php`

```php
...
public function index(Request $request)
{
   $request->user()->authorizeRoles(['user', 'admin']);        
   return view('home'); 
}
...
```

En las vistas también se pueden verificar los roles. Por ejemplo, en el archivo `resources/views/home.blade.php`

```html
@if(Auth::user()->hasRole('admin'))
    <div>Acceso como administrador</div>
@else
    <div>Acceso usuario</div>
@endif
```

### Seeders

Crear seeders Roles

```bash
php artisan make:seeder RolesSeeder
```

Creando seeders User

```bash
php artisan make:seeder UsersSeeder
```

Añadir los seeders al archivo `database/seeds/DatabaseSeeder.php` 

```php
...
public function run()
{
	$this->call(RolesSeeder::class);
    $this->call(UsersSeeder::class);
}
...
```

Editar el archivo `RolesSeeder` en el directorio `database/seeds/RolesSeeder.php`

```php
...
use App\Role;
...
```



```php
...
public function run()
{
	$role = new Role()
    $role->name = 'admin';
    $role->description = 'Administrator';
    $role->save();
    $role = new Role();
    $role->name = 'user';
    $role->description = 'User';
    $role->save();
}
...
```

Editar el archivo  `UsersSeeder` en el directorio `database/seeds/UsersSeeder.php`

```php
...
use App\User;
use App\Role;
...
```

```php
...
public function run()
{
    ...
    $role_user = Role::where('name', 'user')->first();
    $role_admin = Role::where('name', 'admin')->first();
    // User
    $user = new User();
    $user->name = 'User';
    $user->email = 'user@example.com';
    $user->password = bcrypt('secret');
    $user->save();
    $user->roles()->attach($role_user)
    // Admin User
    $user = new User();
    $user->name = 'Admin';
    $user->email = 'admin@example.com';
    $user->password = bcrypt('secret');
    $user->save();
    $user->roles()->attach($role_admin);
    ...
}
...
```

Refrescar migraciones y seeders en la base de datos.

```bash
php artisan migrate:refresh --seed
```

## Creando Middleware Auth

Crear middleware

```bash
php artisan make:middleware CheckRole
```

Editar el middleware  `CheckRole` en el directorio `app/Http/Middleware/CheckRole.php`, añadir la verificación de los roles

 https://stackoverflow.com/questions/43901719/laravel-middleware-with-multiple-roles/43902371 

```php
...
public function handle($request, Closure $next, ... $roles)
{
	foreach($roles as $role) {
    	if (! $request->user()->hasRole($role)) {
        	return $next($request);
        }
    }
        
    return redirect('login');
}
...
```

Registrar el middleware en el archivo `Kernel.php` en la variable `$routeMiddleware`, en el directorio `app/Http/Kernel.php`

```php
protected $routeMiddleware = [
	'role' => \App\Http\Middleware\CheckRole::class,
];
```

Añadir el middleware a las rutas en el directorio `routes/web.php`

```php
Route::post('item', function ($id) {
    //
})->middleware('auth', 'role:admin');

Route::get('/home', 'HomeController@index')->name('home')->middleware('auth', 'role:admin,user');
```

**NOTA:** Lo anterior hace innecesaria la línea de verificación de roles en los métodos

## Sites Feature

Crear modelo

```bash
php artisan make:model SiteAccount -m
```

Editar el modelo `SiteAccount` en el directorio `app/SiteAccount.php`

```php
...
protected $fillable = ['name', 'url', 'pass','user_id'];
...
```

```php
...
public function post()
{
	return $this->belongsTo('App\User');
}
...
```

Editar la migración  en el archivo `database/migrations/2020_01_26_170146_create_site_accounts_table.php`

```php
...
public function up()
{
	Schema::create('site_accounts', function (Blueprint $table) {
        ...
        $table->string('name');
        $table->string('url',1000);
        $table->string('pass',1000);
        $table->integer('user_id')->unsigned();
        $table->timestamps();
        ...
    });
}
...
```

Editar el modelo `User` en el directorio `app/User.php`

```php
...
public function site(){
	return $this
            ->belongsToMany('App\SiteAccount')
            ->withTimestamps();
}
...
```



Ejecutar migraciones

```bash
php artisan migrate
```

Crear controlador

````bash
php artisan make:controller SitesAccountsController --resource
````

### Listar sitios

Editar la clase `SitesAccountsController` en el directorio `app/Http/Controllers/SitesAccountsController.php`

```php
...
use Auth;
use App\SiteAccount;
...
```

```php
...
public function index()
{
	$sites = SiteAccount::latest()->where('user_id', "=", Auth::user()->id)->paginate(6);

    return view('sites.index', compact('sites'))
        ->with('i', (request()->input('page',1)-1)*6);
}
...
```

Crear la respectiva vista `resources/views/sites/index.blade.php`

```html
@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col text-center">
                <a class="btn btn-dark btn-sm" href="{{ route('sites.create') }}"> Create New Site</a>
            </div>
        </div>

        <div class="row">
            @if ($message = Session::get('success'))
                <div class="alert alert-success center-element col-md-12 m-1">
                    <p>{{ $message }}</p>
                </div>
            @endif
        </div>

        <div class="row">
            @foreach ($sites as $site)
                <div class="float-left m-1">
                    <div class="card" style="width: 18rem;">
                        <div class="card-body">
                            <h5 class="card-title"><a href="{{ $site->url }}"> {{ $site->name }} </a>  <img src="{{ $site->url }}/favicon.ico" height="16" width="16"> </h5>

                            <a href="{{ route('sites.show',$site->id) }}" class="btn btn-dark btn-sm">More info</a>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>


        <div class="row">
            <div class="col text-center">
                {!! $sites->links() !!}
            </div>
        </div>
    </div>
@endsection
```

Añadir las rutas en el archivo `routes/web.php`

```php
Route::resource('sites','WebSiteController')->middleware('auth', 'role:user,admin');
```

Visualizar las rutas que existen en la aplicación

```bash
php artisan route:list
```

### Crear Sitio

Editar la clase `SitesAccountsController` en el directorio `app/Http/Controllers/SitesAccountsController.php`

```php
...
public function create()
{
    return view('sites.create');
}
...
```

```php
...
public function store(Request $request)
{
	request()->validate([
    	'name' => 'required',
        'url'  => 'required',
        'pass' => 'required',
    ]);

    $request->request->add(['user_id' => Auth::user()->id]);

    $data = $request->all();
    $data["pass"] = encrypt($data["pass"]);
    SiteAccount::create($data);
    return redirect()->route('sites.index')
            ->with('success','Site created Success');
}
...
```

Crear la respectiva vista `resources/views/sites/create.blade.php`

```html
@extends('layouts.app')

@section('content')
    <div class="container">
        @if ($errors->any())
            <div class="alert alert-danger col-md-12 m-1">
                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Add new site') }}</div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('sites.store') }}">
                            @csrf

                            <div class="form-group row">
                                <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                                <div class="col-md-6">
                                    <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" required autocomplete="name" autofocus>

                                    @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="url" class="col-md-4 col-form-label text-md-right">{{ __('Url') }}</label>

                                <div class="col-md-6">
                                    <input id="url" type="text" class="form-control @error('name') is-invalid @enderror" name="url" required autocomplete="url" autofocus>

                                    @error('url')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="pass" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                                <div class="col-md-6">
                                    <input id="pass" type="text" class="form-control @error('pass') is-invalid @enderror" name="pass" required autocomplete="current-password">

                                    @error('pass')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-8 offset-md-4">
                                    <button type="submit" class="btn btn-primary btn-sm">
                                        {{ __('Save') }}
                                    </button>
                                    
                                    <a class="btn btn-primary btn-sm" href="{{ route('sites.index') }}"> {{ __('Back to list') }}</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
```

### Ver Sitio

Editar la clase `SitesAccountsController` en el directorio `app/Http/Controllers/SitesAccountsController.php`

```php
...
public function show(SiteAccount $site)
{
	$site->pass = decrypt($site->pass);
    return view('sites.show',compact('site'));
}
...
```

Crear la respectiva vista `resources/views/sites/show.blade.php`

```html
@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Info') }}</div>

                <div class="card-body">
                    <div class="form-group row">
                        <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                        <div class="col-md-6">
                            <input id="name" type="name" class="form-control" name="name" value="{{ $site->name }}" disabled autofocus>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="url" class="col-md-4 col-form-label text-md-right">{{ __('Url') }}</label>

                        <div class="col-md-6">
                            <input id="url" type="url" class="form-control" name="url" value="{{ $site->url }}" disabled autofocus>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="pass" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                        <div class="col-md-6">
                            <input id="pass" type="pass" class="form-control" name="pass" value="{{ $site->pass }}" disabled autofocus>
                        </div>
                    </div>
                    <div class="form-group row mb-0">
                        <div class="col-md-8 offset-md-4">
                            <a class="btn btn-primary btn-sm" href="{{ route('sites.index') }}"> {{ __('Back to list') }}</a>

                            <a class="btn btn-warning btn-sm" href="{{ route('sites.edit',$site->id) }}"> {{ __('Edit') }}</a>

    
                            <div class="float-left m-1">
                                <form action="{{ route('sites.destroy',$site->id) }}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-link btn-sm">{{ __('Delete') }}</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
```

### Editar Sitio

Editar la clase `SitesAccountsController` en el directorio `app/Http/Controllers/SitesAccountsController.php`

```php
...
public function edit(SiteAccount $site)
{
	$site->pass = decrypt($site->pass);
    return view('sites.edit',compact('site'));
}
...
```

```php
...
public function update(Request $request, SiteAccount $site)
{
	request()->validate([
    	'name' => 'required',
        'url'  => 'required',
       	'pass' => 'required',
    ]);
        
    $data = $request->all();
    $data["pass"] = encrypt($data["pass"]);
    
    $site->update($data);
    
    return redirect()->route('sites.index')
            ->with('success','Site updated Success');
}
...
```

Crear la respectiva vista `resources/views/sites/edit.blade.php`

```php+HTML
@extends('layouts.app')

@section('content')
    <div class="container">
        @if ($errors->any())
            <div class="alert alert-danger col-md-12 m-1">
                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Edit') }}</div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('sites.update',$site->id) }}">
                            @csrf
                            @method('PUT')

                            <div class="form-group row">
                                <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                                <div class="col-md-6">
                                    <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ $site->name }}" required autocomplete="name" autofocus>

                                    @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="url" class="col-md-4 col-form-label text-md-right">{{ __('Url') }}</label>

                                <div class="col-md-6">
                                    <input id="url" type="text" class="form-control @error('name') is-invalid @enderror" name="url" value="{{ $site->url }}" required autocomplete="url" autofocus>

                                    @error('url')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="pass" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                                <div class="col-md-6">
                                    <input id="pass" type="text" class="form-control @error('pass') is-invalid @enderror" name="pass" value="{{ $site->pass }}" required autocomplete="current-password">

                                    @error('pass')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-8 offset-md-4">
                                    <button type="submit" class="btn btn-primary btn-sm">
                                        {{ __('Save') }}
                                    </button>
                                    
                                    <a class="btn btn-primary btn-sm" href="{{ route('sites.index') }}"> {{ __('Back to list') }}</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
```

### Eliminar Sitio

Editar la clase `SitesAccountsController` en el directorio `app/Http/Controllers/SitesAccountsController.php`

```php
...
public function destroy(SiteAccount $site)
{
	$site->delete();
    return redirect()->route('sites.index')
          	->with('success','Site deleted success');
}
...
```

## Ejecutar proyecto

Clonar repositorio

```bash
git clone git@github.com:brayvasq/mgpass.git
```

Instalar dependencias

```bash
npm install && nmp run dev
composer install
```

Crear base de datos y configurar archivo `.env`

Ejecutar migraciones

```bash
php artisan migrate
```

Ejecutar seeders

```bash
php artisan migrate:refresh --seed
```

Ejecutar proyecto

```bash
php artisan serve
```

## Previews

![Inicio](https://github.com/brayvasq/mgpass/blob/master/storage/app/public/images/list.PNG?raw=true)

![Inicio](https://github.com/brayvasq/mgpass/blob/master/storage/app/public/images/show.PNG?raw=true)

![Inicio](https://github.com/brayvasq/mgpass/blob/master/storage/app/public/images/new.PNG?raw=true)

![Inicio](https://github.com/brayvasq/mgpass/blob/master/storage/app/public/images/edit.PNG?raw=true)

## Referencias

- https://medium.com/modulr/create-scaffold-with-laravel-5-7-f5ab353dff1c
- https://medium.com/@cvallejo/autenticaci%C3%B3n-de-usuarios-y-roles-en-laravel-5-5-97ab59552d91
- https://medium.com/@cvallejo/middleware-roles-en-laravel-5-6-87541406426f