<?php

namespace App\Http\Controllers;

use Auth;
use App\SiteAccount;
use Illuminate\Http\Request;

class SitesAccountsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sites = SiteAccount::latest()->where('user_id', "=", Auth::user()->id)->paginate(6);


        return view('sites.index', compact('sites'))
        ->with('i', (request()->input('page',1)-1)*6);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('sites.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'name' => 'required',
            'url'  => 'required',
            'pass' => 'required',
        ]);

        $request->request->add(['user_id' => Auth::user()->id]);

        $data = $request->all();
        $data["pass"] = encrypt($data["pass"]);
        
        SiteAccount::create($data);

        return redirect()->route('sites.index')
            ->with('success','Site created Success');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(SiteAccount $site)
    {
        $site->pass = decrypt($site->pass);
        return view('sites.show',compact('site'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(SiteAccount $site)
    {
        $site->pass = decrypt($site->pass);
        return view('sites.edit',compact('site'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SiteAccount $site)
    {
        request()->validate([
            'name' => 'required',
            'url'  => 'required',
            'pass' => 'required',
        ]);
        
        $data = $request->all();
        $data["pass"] = encrypt($data["pass"]);

        $site->update($data);
        
        return redirect()->route('sites.index')
            ->with('success','Site updated Success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(SiteAccount $site)
    {
        $site->delete();
        return redirect()->route('sites.index')
            ->with('success','Site deleted success');
    }
}
