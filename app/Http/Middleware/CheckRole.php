<?php

namespace App\Http\Middleware;

use Closure;
use Log;

class CheckRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, ... $roles)
    {
        Log::debug($roles);

        foreach($roles as $role) {
            Log::debug($role);
            Log::debug($request->user()->hasRole($role));
            if (! $request->user()->hasRole($role)) {
                return $next($request);
            }
        }
        
        return redirect('login');
    }
}
