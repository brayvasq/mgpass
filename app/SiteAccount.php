<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SiteAccount extends Model
{
    /**
     * Field to be mass-assigned.
     *
     * @var array
     */
    protected $fillable = ['name', 'url', 'pass','user_id'];

    /**
     * Get the user that owns the site.
     */
    public function post()
    {
        return $this->belongsTo('App\User');
    }
}
