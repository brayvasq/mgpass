@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col text-center">
                <a class="btn btn-dark btn-sm" href="{{ route('sites.create') }}"> Create New Site</a>
            </div>
        </div>

        <div class="row">
            @if ($message = Session::get('success'))
                <div class="alert alert-success center-element col-md-12 m-1">
                    <p>{{ $message }}</p>
                </div>
            @endif
        </div>

        <div class="row">
            @foreach ($sites as $site)
                <div class="float-left m-1">
                    <div class="card" style="width: 18rem;">
                        <div class="card-body">
                            <h5 class="card-title"><a href="{{ $site->url }}"> {{ $site->name }} </a>  <img src="{{ $site->url }}/favicon.ico" height="16" width="16"> </h5>

                            <a href="{{ route('sites.show',$site->id) }}" class="btn btn-dark btn-sm">More info</a>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>


        <div class="row">
            <div class="col text-center">
                {!! $sites->links() !!}
            </div>
        </div>
    </div>
@endsection