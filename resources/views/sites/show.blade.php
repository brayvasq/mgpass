@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Info') }}</div>

                <div class="card-body">
                    <div class="form-group row">
                        <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                        <div class="col-md-6">
                            <input id="name" type="name" class="form-control" name="name" value="{{ $site->name }}" disabled autofocus>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="url" class="col-md-4 col-form-label text-md-right">{{ __('Url') }}</label>

                        <div class="col-md-6">
                            <input id="url" type="url" class="form-control" name="url" value="{{ $site->url }}" disabled autofocus>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="pass" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                        <div class="col-md-6">
                            <input id="pass" type="pass" class="form-control" name="pass" value="{{ $site->pass }}" disabled autofocus>
                        </div>
                    </div>
                    <div class="form-group row mb-0">
                        <div class="col-md-8 offset-md-4">
                            <a class="btn btn-primary btn-sm" href="{{ route('sites.index') }}"> {{ __('Back to list') }}</a>

                            <a class="btn btn-warning btn-sm" href="{{ route('sites.edit',$site->id) }}"> {{ __('Edit') }}</a>

    
                            <div class="float-left m-1">
                                <form action="{{ route('sites.destroy',$site->id) }}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-link btn-sm">{{ __('Delete') }}</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
